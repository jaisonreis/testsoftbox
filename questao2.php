<?php

class DbConnection {

    private $user;
    private $pass;
    private $host;
    private $dbname;

    private $connection;

    private static $defaultConnection = null;

    public function __construct($dbname, $user, $pass, $host) {
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
        $this->dbname = $dbname;
    }

    public function getConnection() {
        if($this->connection == null) {
            $this->connection = mysql_pconnect($this->host, $this->user, $this->pass, true);
            mysql_select_db($this->dbname, $this->connect());
        }
        return $this->connection;
    }

    /**
     * @return DbConnection
     */
    public static function getDefaultConnection() {
        if(self::$defaultConnection === null) {
            self::$defaultConnection = new self(DB_NAME, DB_USER, DB_PASS, DB_HOST);
        }
        return self::$defaultConnection;
    }

    public function disconnect() {
        mysql_close($this->getConnection());
    }
    
}

class Model {
    protected $connection;

    public function __construct(DbConnection $connection) {
        $this->connection = $connection;
    }

    public function query()
    {
        // Não precisa implementar
    }
}

// Escopo A
$m = new Model(DbConnection::getDefaultConnection());
$m->query("...");

// Escopo B
$m = new Model(DbConnection::getDefaultConnection());
$m->query("...");