<?php

namespace Softbox\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PessoaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome', 'text', array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Length(array(
                    'max' => 30
                )))
        ));

        $idadeOptions = array(
            'Entre 0 e 18'=>'Entre 0 e 18',
            'Entre 19 e 25'=>'Entre 19 e 25',
            'Entre 26 e 35'=>'Entre 26 e 35',
            '36 ou Mais'=>'36 ou Mais'
        );

        $builder->add('idade', 'choice', array(
            'empty_value' => 'Selecione',
            'choices' => $idadeOptions,
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Choice(array(
                    'choices' => $idadeOptions
                ))
            )
        ));


        $sexoOptions = array(
            'Masculino' => 'Masculino',
            'Feminino' => 'Feminino'
        );
        $builder->add('sexo', 'choice', array(
            'choices' => $sexoOptions,
            'expanded' => true,
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Choice(array(
                    'choices' => $sexoOptions
                ))
            )
        ));

        $builder->add('email', 'email', array(
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Email()
            )
        ));

        $builder->add('salvar', 'submit');

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'required' => true
        ));
    }

    public function getName()
    {
        return 'pessoa';
    }

}