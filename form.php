<?php
/**
 * Esta é uma das possíveis soluções para a questão
 * 1 do teste da Softbox. Nesta solução utilizei o
 * microframework "Silex" por ser um framework completo
 * mas com pouco volume de código.
 *
 * Meu principal objetivo foi facilitar a renderização do
 * formulário e tratamento de erros bem como pensar
 * na futura extensibilidade deste código (simulando
 * o ambiente de uma aplicação real.
 *
 * Meu ambiente de desenvolvimento utiliza a versão 5.4 do
 * PHP. Este exemplo deve funcionar a partir do PHP 5.3
 * mas não no PHP 5.2, principalmente pelo uso de Closures.
 */

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

$app = new Silex\Application();
$app['debug'] = true;
$app['locale'] = 'pt_BR';

// Registra o Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => array(__DIR__ . '/src/Softbox/views',__DIR__ . '/src/Softbox/views/form'),
    'twig.form.templates' => array(
        'form_row.twig'
    )
));

$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());

$app->match('/', function(Request $request) use ($app) {

    /* @var $formFactory Symfony\Component\Form\FormFactory */
    $formFactory = $app['form.factory'];

    $form = $formFactory->createBuilder(new \Softbox\Form\PessoaType())->getForm();

    $isValid = null;

    if('POST' == $request->getMethod()) {
        $form->submit($request);


        if($form->isValid()) {
            $data = $form->getData();
            $isValid = true;
        }
    }

    return $app['twig']->render('form.html.twig', array(
        'form' => $form->createView(),
        'isValid' => $isValid
    ));
});


$app->run();